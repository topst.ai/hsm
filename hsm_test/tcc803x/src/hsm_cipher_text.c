/*******************************************************************************

*   Copyright (c) Telechips Inc.


*   TCC Version 1.0

This source code contains confidential information of Telechips.

Any unauthorized use without a written permission of Telechips including not
limited to re-distribution in source or binary form is strictly prohibited.

This source code is provided "AS IS" and nothing contained in this source code
shall constitute any express or implied warranty of any kind, including without
limitation, any warranty of merchantability, fitness for a particular purpose
or non-infringement of any patent, copyright or other third party intellectual
property right.
No warranty is made, express or implied, regarding the information's accuracy,
completeness, or performance.

In no event shall Telechips be liable for any claim, damages or other
liability arising from, out of or in connection with this source code or
the use in the source code.

This source code is provided subject to the terms of a Mutual Non-Disclosure
Agreement between Telechips and Company.
*
*******************************************************************************/
#include <stdint.h>

// clang-format off

/* AES Test */
uint8_t AES128_1_1001[1] = {
    0x6b,
};
uint8_t AES128_1_1002[1] = {
    0xf,
};
uint8_t AES128_16_0000[16] = {
    0x6b, 0x3c, 0x4a, 0x27, 0x7a, 0x28, 0xba, 0xa5, 0x59, 0x35, 0x6a, 0xcb, 0x55, 0xe1, 0xfc, 0x6c,
};
uint8_t AES128_16_1100[16] = {
    0x30, 0x7a, 0xec, 0xf8, 0x7f, 0x89, 0xf1, 0xa1, 0xe2, 0x5f, 0xa3, 0xec, 0x18, 0x6c, 0x13, 0x47,
};
uint8_t AES128_16_1110[16] = {
    0x30, 0x7a, 0xec, 0xf8, 0x7f, 0x89, 0xf1, 0xa1, 0xe2, 0x5f, 0xa3, 0xec, 0x18, 0x6c, 0x13, 0x47,
};
uint8_t AES128_16_1122[16] = {
    0x30, 0x7a, 0xec, 0xf8, 0x7f, 0x89, 0xf1, 0xa1, 0xe2, 0x5f, 0xa3, 0xec, 0x18, 0x6c, 0x13, 0x47,
};
uint8_t AES128_16_1433[16] = {
    0x6b, 0x79, 0x35, 0xb2, 0xb5, 0x76, 0x69, 0xd5, 0x69, 0x1f, 0xce, 0x10, 0x5, 0x80, 0x92, 0xef,
};
uint8_t AES128_16_1533[16] = {
    0x6b, 0x79, 0x35, 0xb2, 0xb5, 0x76, 0x69, 0xd5, 0x69, 0x1f, 0xce, 0x10, 0x5, 0x80, 0x92, 0xef,
};
uint8_t AES128_17_0000[17] = {
    0x6b, 0x3c, 0x4a, 0x27, 0x7a, 0x28, 0xba, 0xa5, 0x59,
    0x35, 0x6a, 0xcb, 0x55, 0xe1, 0xfc, 0x6c, 0x20,
};
uint8_t AES128_17_1100[17] = {
    0x30, 0x7a, 0xec, 0xf8, 0x7f, 0x89, 0xf1, 0xa1, 0xe2,
    0x5f, 0xa3, 0xec, 0x18, 0x6c, 0x13, 0x47, 0x20,
};
uint8_t AES128_17_1110[17] = {
    0x18, 0xc2, 0xd7, 0x98, 0xe7, 0xec, 0x75, 0x51, 0xc4,
    0xee, 0x4b, 0xae, 0x42, 0xb2, 0x98, 0xb3, 0x30,
};
uint8_t AES128_17_1122[17] = {
    0x30, 0x7a, 0xec, 0xf8, 0x7f, 0x89, 0xf1, 0xa1, 0xe2,
    0x5f, 0xa3, 0xec, 0x18, 0x6c, 0x13, 0x47, 0x58,
};
uint8_t AES128_17_1433[17] = {
    0x6b, 0x79, 0x35, 0xb2, 0xb5, 0x76, 0x69, 0xd5, 0x69,
    0x1f, 0xce, 0x10, 0x5,  0x80, 0x92, 0xef, 0x15,
};
uint8_t AES128_17_1533[17] = {
    0x6b, 0x79, 0x35, 0xb2, 0xb5, 0x76, 0x69, 0xd5, 0x69,
    0x1f, 0xce, 0x10, 0x5,  0x80, 0x92, 0xef, 0x15,
};
uint8_t AES128_32_0000[32] = {
    0x6b, 0x3c, 0x4a, 0x27, 0x7a, 0x28, 0xba, 0xa5, 0x59, 0x35, 0x6a, 0xcb, 0x55, 0xe1, 0xfc, 0x6c,
    0x2a, 0xfa, 0x3a, 0xcd, 0x41, 0xf5, 0x40, 0x61, 0x95, 0xfe, 0xa2, 0xf7, 0xb5, 0xb2, 0x4c, 0x9a,
};
uint8_t AES128_32_1033[32] = {
    0x6b, 0x79, 0x35, 0xb2, 0xb5, 0x76, 0x69, 0xd5, 0x69, 0x1f, 0xce, 0x10, 0x5,  0x80, 0x92, 0xef,
    0x15, 0xd0, 0xfb, 0xfd, 0xac, 0x22, 0x64, 0x2a, 0x94, 0x69, 0xd9, 0x25, 0x60, 0xde, 0xf7, 0xa7,
};
uint8_t AES128_32_1100[32] = {
    0x30, 0x7a, 0xec, 0xf8, 0x7f, 0x89, 0xf1, 0xa1, 0xe2, 0x5f, 0xa3, 0xec, 0x18, 0x6c, 0x13, 0x47,
    0xe1, 0xa8, 0x9,  0xce, 0xe7, 0xb0, 0x41, 0x91, 0x6c, 0x51, 0x3,  0xf5, 0x4f, 0x70, 0x6,  0x3a,
};
uint8_t AES128_32_1110[32] = {
    0x30, 0x7a, 0xec, 0xf8, 0x7f, 0x89, 0xf1, 0xa1, 0xe2, 0x5f, 0xa3, 0xec, 0x18, 0x6c, 0x13, 0x47,
    0xe1, 0xa8, 0x9,  0xce, 0xe7, 0xb0, 0x41, 0x91, 0x6c, 0x51, 0x3,  0xf5, 0x4f, 0x70, 0x6,  0x3a,
};
uint8_t AES128_32_1122[32] = {
    0x30, 0x7a, 0xec, 0xf8, 0x7f, 0x89, 0xf1, 0xa1, 0xe2, 0x5f, 0xa3, 0xec, 0x18, 0x6c, 0x13, 0x47,
    0xe1, 0xa8, 0x9,  0xce, 0xe7, 0xb0, 0x41, 0x91, 0x6c, 0x51, 0x3,  0xf5, 0x4f, 0x70, 0x6,  0x3a,
};
uint8_t AES128_32_1433[32] = {
    0x6b, 0x79, 0x35, 0xb2, 0xb5, 0x76, 0x69, 0xd5, 0x69, 0x1f, 0xce, 0x10, 0x5,  0x80, 0x92, 0xef,
    0x15, 0xd0, 0xfb, 0xfd, 0xac, 0x22, 0x64, 0x2a, 0x94, 0x69, 0xd9, 0x25, 0x60, 0xde, 0xf7, 0xa7,
};
uint8_t AES128_32_1533[32] = {
    0x6b, 0x79, 0x35, 0xb2, 0xb5, 0x76, 0x69, 0xd5, 0x69, 0x1f, 0xce, 0x10, 0x5,  0x80, 0x92, 0xef,
    0x15, 0xd0, 0xfb, 0xfd, 0xac, 0x22, 0x64, 0x2a, 0x94, 0x69, 0xd9, 0x25, 0x60, 0xde, 0xf7, 0xa7,
};
uint8_t AES128_7_1001[7] = {
    0x6b, 0x79, 0x35, 0xb2, 0xb5, 0x76, 0x69,
};
uint8_t AES128_7_1002[7] = {
    0xf, 0xe1, 0x99, 0xd0, 0xd1, 0xb0, 0x8f,
};

/* DES Test */
uint8_t DES_1_1001[1] = {
    0x1b,
};
uint8_t DES_1_1002[1] = {
    0x67,
};
uint8_t DES_16_0000[16] = {
    0xda, 0xd7, 0x75, 0x99, 0x97, 0x66, 0xb9, 0xd9, 0x56, 0x4, 0x7a, 0x6b, 0x16, 0xf6, 0xc1, 0xe0,
};
uint8_t DES_16_0010[16] = {
    0xda, 0xd7, 0x75, 0x99, 0x97, 0x66, 0xb9, 0xd9, 0x56, 0x4, 0x7a, 0x6b, 0x16, 0xf6, 0xc1, 0xe0,
};
uint8_t DES_16_0020[16] = {
    0xda, 0xd7, 0x75, 0x99, 0x97, 0x66, 0xb9, 0xd9, 0x56, 0x4, 0x7a, 0x6b, 0x16, 0xf6, 0xc1, 0xe0,
};
uint8_t DES_16_1100[16] = {
    0x5b, 0xec, 0xc7, 0x3d, 0xda, 0xfe, 0xc, 0x6a, 0x29, 0xdd, 0x92, 0x8c, 0x5b, 0xe9, 0xbd, 0x78,
};
uint8_t DES_16_1130[16] = {
    0x5b, 0xec, 0xc7, 0x3d, 0xda, 0xfe, 0xc, 0x6a, 0x29, 0xdd, 0x92, 0x8c, 0x5b, 0xe9, 0xbd, 0x78,
};
uint8_t DES_16_1142[16] = {
    0x5b, 0xec, 0xc7, 0x3d, 0xda, 0xfe, 0xc, 0x6a, 0x29, 0xdd, 0x92, 0x8c, 0x5b, 0xe9, 0xbd, 0x78,
};
uint8_t DES_17_0000[17] = {
    0xda, 0xd7, 0x75, 0x99, 0x97, 0x66, 0xb9, 0xd9, 0x56,
    0x4,  0x7a, 0x6b, 0x16, 0xf6, 0xc1, 0xe0, 0x20,
};
uint8_t DES_17_0010[17] = {
    0xda, 0xd7, 0x75, 0x99, 0x97, 0x66, 0xb9, 0xd9, 0x54,
    0x48, 0xb1, 0x9e, 0xd8, 0xd2, 0x38, 0x7a, 0x56,
};
uint8_t DES_17_0020[17] = {
    0xda, 0xd7, 0x75, 0x99, 0x97, 0x66, 0xb9, 0xd9, 0x40,
    0x5e, 0x24, 0x8b, 0x11, 0x89, 0xa4, 0x5f, 0x81,
};
uint8_t DES_17_1100[17] = {
    0x5b, 0xec, 0xc7, 0x3d, 0xda, 0xfe, 0xc,  0x6a, 0x29,
    0xdd, 0x92, 0x8c, 0x5b, 0xe9, 0xbd, 0x78, 0x20,
};
uint8_t DES_17_1130[17] = {
    0x5b, 0xec, 0xc7, 0x3d, 0xda, 0xfe, 0xc,  0x6a, 0x9f,
    0xb6, 0xed, 0xdb, 0x10, 0x8f, 0x27, 0x2c, 0x29,
};
uint8_t DES_17_1142[17] = {
    0x5b, 0xec, 0xc7, 0x3d, 0xda, 0xfe, 0xc,  0x6a, 0x29,
    0xdd, 0x92, 0x8c, 0x5b, 0xe9, 0xbd, 0x78, 0x71,
};
uint8_t DES_7_1001[7] = {
    0x1b, 0x9e, 0x90, 0xa5, 0xd3, 0x5a, 0x9e,
};
uint8_t DES_7_1002[7] = {
    0x67, 0xcc, 0x75, 0x1e, 0x81, 0x9b, 0x2c,
};
uint8_t DES_8_0000[8] = {
    0xda, 0xd7, 0x75, 0x99, 0x97, 0x66, 0xb9, 0xd9,
};
uint8_t DES_8_0010[8] = {
    0xda, 0xd7, 0x75, 0x99, 0x97, 0x66, 0xb9, 0xd9,
};
uint8_t DES_8_0020[8] = {
    0xda, 0xd7, 0x75, 0x99, 0x97, 0x66, 0xb9, 0xd9,
};
uint8_t DES_8_1100[8] = {
    0x5b, 0xec, 0xc7, 0x3d, 0xda, 0xfe, 0xc, 0x6a,
};
uint8_t DES_8_1130[8] = {
    0x5b, 0xec, 0xc7, 0x3d, 0xda, 0xfe, 0xc, 0x6a,
};
uint8_t DES_8_1142[8] = {
    0x5b, 0xec, 0xc7, 0x3d, 0xda, 0xfe, 0xc, 0x6a,
};

/* MULTI2 Test */
uint8_t Multi2_1_1001[1] = {
    0xe3,
};
uint8_t Multi2_1_1002[1] = {
    0x1c,
};
uint8_t Multi2_16_0000[16] = {
    0xfc, 0xaa, 0x8d, 0xc7, 0x93, 0x4, 0xee, 0x57, 0xf6, 0x18, 0xb9, 0x2c, 0x52, 0xe0, 0x2c, 0x5f,
};
uint8_t Multi2_16_0010[16] = {
    0xfc, 0xaa, 0x8d, 0xc7, 0x93, 0x4, 0xee, 0x57, 0xf6, 0x18, 0xb9, 0x2c, 0x52, 0xe0, 0x2c, 0x5f,
};
uint8_t Multi2_16_0020[16] = {
    0xfc, 0xaa, 0x8d, 0xc7, 0x93, 0x4, 0xee, 0x57, 0xf6, 0x18, 0xb9, 0x2c, 0x52, 0xe0, 0x2c, 0x5f,
};
uint8_t Multi2_16_1100[16] = {
    0xe5, 0xb0, 0x54, 0xc, 0x74, 0x86, 0xe3, 0xdf, 0x93, 0xca, 0x6e, 0xc7, 0x47, 0x54, 0xd0, 0x5b,
};
uint8_t Multi2_16_1130[16] = {
    0xe5, 0xb0, 0x54, 0xc, 0x74, 0x86, 0xe3, 0xdf, 0x93, 0xca, 0x6e, 0xc7, 0x47, 0x54, 0xd0, 0x5b,
};
uint8_t Multi2_16_1142[16] = {
    0xe5, 0xb0, 0x54, 0xc, 0x74, 0x86, 0xe3, 0xdf, 0x93, 0xca, 0x6e, 0xc7, 0x47, 0x54, 0xd0, 0x5b,
};
uint8_t Multi2_17_0000[17] = {
    0xfc, 0xaa, 0x8d, 0xc7, 0x93, 0x4,  0xee, 0x57, 0xf6,
    0x18, 0xb9, 0x2c, 0x52, 0xe0, 0x2c, 0x5f, 0x20,
};
uint8_t Multi2_17_0010[17] = {
    0xfc, 0xaa, 0x8d, 0xc7, 0x93, 0x4,  0xee, 0x57, 0x32,
    0x86, 0xc9, 0xfb, 0x2d, 0x10, 0x91, 0x2b, 0xf6,
};
uint8_t Multi2_17_0020[17] = {
    0xfc, 0xaa, 0x8d, 0xc7, 0x93, 0x4,  0xee, 0x57, 0x7f,
    0x7,  0x1,  0xf1, 0x2b, 0x9b, 0xdc, 0x11, 0xd,
};
uint8_t Multi2_17_1100[17] = {
    0xe5, 0xb0, 0x54, 0xc,  0x74, 0x86, 0xe3, 0xdf, 0x93,
    0xca, 0x6e, 0xc7, 0x47, 0x54, 0xd0, 0x5b, 0x20,
};
uint8_t Multi2_17_1130[17] = {
    0xe5, 0xb0, 0x54, 0xc,  0x74, 0x86, 0xe3, 0xdf, 0xfe,
    0xf8, 0xce, 0xcc, 0xb0, 0x3f, 0xc9, 0xf8, 0x93,
};
uint8_t Multi2_17_1142[17] = {
    0xe5, 0xb0, 0x54, 0xc,  0x74, 0x86, 0xe3, 0xdf, 0x93,
    0xca, 0x6e, 0xc7, 0x47, 0x54, 0xd0, 0x5b, 0xcb,
};
uint8_t Multi2_7_1001[7] = {
    0xe3, 0x47, 0xb8, 0xba, 0x4d, 0x7c, 0xa5,
};
uint8_t Multi2_7_1002[7] = {
    0x1c, 0xa7, 0xa0, 0xde, 0x14, 0x53, 0x2d,
};
uint8_t Multi2_8_0000[8] = {
    0xfc, 0xaa, 0x8d, 0xc7, 0x93, 0x4, 0xee, 0x57,
};
uint8_t Multi2_8_0010[8] = {
    0xfc, 0xaa, 0x8d, 0xc7, 0x93, 0x4, 0xee, 0x57,
};
uint8_t Multi2_8_0020[8] = {
    0xfc, 0xaa, 0x8d, 0xc7, 0x93, 0x4, 0xee, 0x57,
};
uint8_t Multi2_8_1100[8] = {
    0xe5, 0xb0, 0x54, 0xc, 0x74, 0x86, 0xe3, 0xdf,
};
uint8_t Multi2_8_1130[8] = {
    0xe5, 0xb0, 0x54, 0xc, 0x74, 0x86, 0xe3, 0xdf,
};
uint8_t Multi2_8_1142[8] = {
    0xe5, 0xb0, 0x54, 0xc, 0x74, 0x86, 0xe3, 0xdf,
};

/* TDES Test */
uint8_t TDES128_1_1001[1] = {
    0x66,
};
uint8_t TDES128_1_1002[1] = {
    0xdd,
};
uint8_t TDES128_16_0000[16] = {
    0x78, 0x4, 0xf2, 0x20, 0x18, 0xf2, 0x8a, 0x95, 0x11, 0xf0, 0x99, 0x70, 0x98, 0x60, 0xda, 0xab,
};
uint8_t TDES128_16_0010[16] = {
    0x78, 0x4, 0xf2, 0x20, 0x18, 0xf2, 0x8a, 0x95, 0x11, 0xf0, 0x99, 0x70, 0x98, 0x60, 0xda, 0xab,
};
uint8_t TDES128_16_0020[16] = {
    0x78, 0x4, 0xf2, 0x20, 0x18, 0xf2, 0x8a, 0x95, 0x11, 0xf0, 0x99, 0x70, 0x98, 0x60, 0xda, 0xab,
};
uint8_t TDES128_16_1100[16] = {
    0x98, 0x5f, 0xc4, 0xc8, 0xe9, 0xd6, 0x46, 0x73, 0xa6, 0x7f, 0x48, 0x77, 0xed, 0x4c, 0xec, 0xb,
};
uint8_t TDES128_16_1130[16] = {
    0x98, 0x5f, 0xc4, 0xc8, 0xe9, 0xd6, 0x46, 0x73, 0xa6, 0x7f, 0x48, 0x77, 0xed, 0x4c, 0xec, 0xb,
};
uint8_t TDES128_16_1142[16] = {
    0x98, 0x5f, 0xc4, 0xc8, 0xe9, 0xd6, 0x46, 0x73, 0xa6, 0x7f, 0x48, 0x77, 0xed, 0x4c, 0xec, 0xb,
};
uint8_t TDES128_17_0000[17] = {
    0x78, 0x4,  0xf2, 0x20, 0x18, 0xf2, 0x8a, 0x95, 0x11,
    0xf0, 0x99, 0x70, 0x98, 0x60, 0xda, 0xab, 0x20,
};
uint8_t TDES128_17_0010[17] = {
    0x78, 0x4,  0xf2, 0x20, 0x18, 0xf2, 0x8a, 0x95, 0xb1,
    0xab, 0x41, 0x29, 0x13, 0x33, 0x5a, 0x5b, 0x11,
};
uint8_t TDES128_17_0020[17] = {
    0x78, 0x4,  0xf2, 0x20, 0x18, 0xf2, 0x8a, 0x95, 0x49,
    0xfe, 0xd0, 0xac, 0x8d, 0x4,  0xea, 0x90, 0x4e,
};
uint8_t TDES128_17_1100[17] = {
    0x98, 0x5f, 0xc4, 0xc8, 0xe9, 0xd6, 0x46, 0x73, 0xa6,
    0x7f, 0x48, 0x77, 0xed, 0x4c, 0xec, 0xb,  0x20,
};
uint8_t TDES128_17_1130[17] = {
    0x98, 0x5f, 0xc4, 0xc8, 0xe9, 0xd6, 0x46, 0x73, 0x97,
    0x5,  0xed, 0xe3, 0x17, 0x3f, 0x71, 0xa5, 0xa6,
};
uint8_t TDES128_17_1142[17] = {
    0x98, 0x5f, 0xc4, 0xc8, 0xe9, 0xd6, 0x46, 0x73, 0xa6,
    0x7f, 0x48, 0x77, 0xed, 0x4c, 0xec, 0xb,  0xf0,
};
uint8_t TDES128_32_0000[32] = {
    0x78, 0x4, 0xf2, 0x20, 0x18, 0xf2, 0x8a, 0x95, 0x11, 0xf0, 0x99, 0x70, 0x98, 0x60, 0xda, 0xab,
    0xbd, 0x3, 0x17, 0xd2, 0x2e, 0xb1, 0xc8, 0xc6, 0xfb, 0x54, 0x8f, 0xea, 0xf7, 0x1e, 0x3,  0x9f,
};
uint8_t TDES128_32_0010[32] = {
    0x78, 0x4, 0xf2, 0x20, 0x18, 0xf2, 0x8a, 0x95, 0x11, 0xf0, 0x99, 0x70, 0x98, 0x60, 0xda, 0xab,
    0xbd, 0x3, 0x17, 0xd2, 0x2e, 0xb1, 0xc8, 0xc6, 0xfb, 0x54, 0x8f, 0xea, 0xf7, 0x1e, 0x3,  0x9f,
};
uint8_t TDES128_32_0020[32] = {
    0x78, 0x4, 0xf2, 0x20, 0x18, 0xf2, 0x8a, 0x95, 0x11, 0xf0, 0x99, 0x70, 0x98, 0x60, 0xda, 0xab,
    0xbd, 0x3, 0x17, 0xd2, 0x2e, 0xb1, 0xc8, 0xc6, 0xfb, 0x54, 0x8f, 0xea, 0xf7, 0x1e, 0x3,  0x9f,
};
uint8_t TDES128_32_1100[32] = {
    0x98, 0x5f, 0xc4, 0xc8, 0xe9, 0xd6, 0x46, 0x73, 0xa6, 0x7f, 0x48, 0x77, 0xed, 0x4c, 0xec, 0xb,
    0x61, 0x6f, 0xc7, 0x6a, 0x9,  0x65, 0xab, 0x25, 0xd3, 0xda, 0xc,  0xea, 0x95, 0xa5, 0xf3, 0x76,
};
uint8_t TDES128_32_1130[32] = {
    0x98, 0x5f, 0xc4, 0xc8, 0xe9, 0xd6, 0x46, 0x73, 0xa6, 0x7f, 0x48, 0x77, 0xed, 0x4c, 0xec, 0xb,
    0x61, 0x6f, 0xc7, 0x6a, 0x9,  0x65, 0xab, 0x25, 0xd3, 0xda, 0xc,  0xea, 0x95, 0xa5, 0xf3, 0x76,
};
uint8_t TDES128_32_1142[32] = {
    0x98, 0x5f, 0xc4, 0xc8, 0xe9, 0xd6, 0x46, 0x73, 0xa6, 0x7f, 0x48, 0x77, 0xed, 0x4c, 0xec, 0xb,
    0x61, 0x6f, 0xc7, 0x6a, 0x9,  0x65, 0xab, 0x25, 0xd3, 0xda, 0xc,  0xea, 0x95, 0xa5, 0xf3, 0x76,
};
uint8_t TDES128_7_1001[7] = {
    0x66, 0x56, 0x3e, 0x44, 0x95, 0x62, 0x46,
};
uint8_t TDES128_7_1002[7] = {
    0xdd, 0x7f, 0x27, 0x17, 0xd2, 0x6b, 0x77,
};

/* Plain Text */
uint8_t Plain_Text_0000_1[1] = {
    0x49,
};
uint8_t Plain_Text_0000_15[15] = {
    0x49, 0x73, 0x20, 0x74, 0x68, 0x69, 0x73, 0x20, 0x74, 0x68, 0x65, 0x20, 0x72, 0x65, 0x61,
};
uint8_t Plain_Text_0000_16[16] = {
    0x49, 0x73, 0x20, 0x74, 0x68, 0x69, 0x73, 0x20, 0x74, 0x68, 0x65, 0x20, 0x72, 0x65, 0x61, 0x6c,
};
uint8_t Plain_Text_0000_17[17] = {
    0x49, 0x73, 0x20, 0x74, 0x68, 0x69, 0x73, 0x20, 0x74,
    0x68, 0x65, 0x20, 0x72, 0x65, 0x61, 0x6c, 0x20,
};
uint8_t Plain_Text_0000_32[32] = {
    0x49, 0x73, 0x20, 0x74, 0x68, 0x69, 0x73, 0x20, 0x74, 0x68, 0x65, 0x20, 0x72, 0x65, 0x61, 0x6c,
    0x20, 0x6c, 0x69, 0x66, 0x65, 0x3f, 0x20, 0x49, 0x73, 0x20, 0x74, 0x68, 0x69, 0x73, 0x20, 0x6a,
};
uint8_t Plain_Text_0000_7[7] = {
    0x49, 0x73, 0x20, 0x74, 0x68, 0x69, 0x73,
};
uint8_t Plain_Text_0000_8[8] = {
    0x49, 0x73, 0x20, 0x74, 0x68, 0x69, 0x73, 0x20,
};

uint8_t DIN0[16] = {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
                                 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF};
uint8_t DIN1[16] = {0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88,
                                 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00};
uint8_t DIN2[16] = {0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99,
                                 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00, 0x11};
uint8_t DIN3[16] = {0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA,
                                 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00, 0x11, 0x22};
uint8_t DIN4[16] = {0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB,
                                 0xCC, 0xDD, 0xEE, 0xFF, 0x00, 0x11, 0x22, 0x33};
uint8_t DIN5[16] = {0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD,
                                 0xEE, 0xFF, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55};
uint8_t Nonce[16] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
uint8_t FOR_KL_PLAINTEXT[16] = {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
                                             0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF};
uint8_t TDES_CBC_IV_FOR_KL[8] = {0x12, 0x34, 0x56, 0x78, 0x9a, 0xbc, 0xde, 0xf0};
uint8_t TDES_ECB_FOR_KL_CIPHERTEXT[16] = {
    0xFD, 0xEB, 0x0D, 0x55, 0xE1, 0x2D, 0x33, 0x00, 0x4B, 0xC1, 0xA1, 0x93, 0xCA, 0x1D, 0x08, 0x67};
uint8_t TDES_CBC_FOR_KL_CIPHERTEXT[16] = {
    0x70, 0xB6, 0xC6, 0x7F, 0xD8, 0x3B, 0x39, 0x95, 0x42, 0x46, 0x46, 0x4A, 0x64, 0xF4, 0xC8, 0x46};
uint8_t AES_CBC_IV_FOR_KL[16] = {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
                                              0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff};
uint8_t AES_ECB_FOR_KL_CIPHERTEXT[16] = {
    0x23, 0x64, 0xD5, 0x28, 0x5E, 0x01, 0x0E, 0xB8, 0x93, 0xED, 0xB3, 0xD0, 0x2D, 0xD6, 0x8C, 0xC8};
uint8_t AES_CBC_FOR_KL_CIPHERTEXT[16] = {
    0x31, 0x0D, 0x69, 0xB6, 0x92, 0xBA, 0x02, 0x39, 0x6F, 0x3E, 0xEB, 0xE0, 0x3B, 0xCB, 0x0F, 0x66};
uint8_t TDES_VID[16] = {0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x2a, 0x42,
                                     0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x2a, 0x42};
uint8_t AES_VID[16] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x2a, 0x42};
uint8_t TDES_MID[16] = {0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xA5,
                                     0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xA5};
uint8_t AES_MID[16] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xA5};
uint8_t KL_DIN1[16] = {0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27,
                                    0x28, 0x29, 0x2A, 0x2B, 0x2C, 0x2D, 0x2E, 0x2F};
uint8_t KL_DIN2[16] = {0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
                                    0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F};
uint8_t KL0_DIN3[16] = {0xD5, 0x38, 0xE6, 0x01, 0x74, 0x41, 0x48, 0x8D,
                                     0xD2, 0x12, 0xA4, 0xF1, 0x69, 0x7E, 0xF0, 0x52};
uint8_t KL1_DIN3[16] = {0xAC, 0x43, 0xD0, 0x60, 0x78, 0x68, 0x9D, 0x12,
                                     0x46, 0x0B, 0x53, 0x0A, 0xFF, 0x3E, 0x09, 0xE4};
uint8_t KL2_DIN3[16] = {0x5F, 0x5F, 0x40, 0x3A, 0xE2, 0x60, 0xCD, 0x2B,
                                     0x6B, 0x62, 0x81, 0x0D, 0x18, 0xF9, 0x65, 0x03};
uint8_t KL3_DIN3[16] = {0x89, 0xBB, 0x35, 0xF7, 0xBA, 0x73, 0xBB, 0x72,
                                     0x62, 0xA3, 0xFD, 0x42, 0x74, 0x7E, 0xEE, 0xCB};
uint8_t KL4_DIN3[16] = {0xD9, 0x37, 0x7D, 0x6E, 0x51, 0x9D, 0x32, 0xE3,
                                     0x4D, 0xB8, 0x26, 0x0F, 0xA5, 0x37, 0x60, 0xEC};
uint8_t KL5_DIN3[16] = {0xB3, 0xA9, 0x5B, 0x27, 0xDC, 0x86, 0x7E, 0x38,
                                     0xC9, 0xA8, 0xF8, 0xD0, 0x2E, 0xF6, 0x26, 0x55};
uint8_t KL6_DIN3[16] = {0xB3, 0xA9, 0x5B, 0x27, 0xDC, 0x86, 0x7E, 0x38,
                                     0xC9, 0xA8, 0xF8, 0xD0, 0x2E, 0xF6, 0x26, 0x55};
uint8_t KL_NONCE_INPUT[16] = {0xA0, 0xA1, 0xA2, 0xA3, 0xA4, 0xA5, 0xA6, 0xA7,
                                           0xA8, 0xA9, 0xAA, 0xAB, 0xAC, 0xAD, 0xAE, 0xAF};

uint8_t KL_WithKDF_CIPHERTEXT[8][16] = {
    {0xC1, 0x35, 0xCB, 0xB1, 0xBB, 0xF0, 0xB7, 0x65, 0x9E, 0x38, 0xCC, 0xEE, 0xDE, 0xB5, 0x8A,
     0x8E},
    {0xCB, 0x5B, 0xAA, 0x80, 0xFB, 0x18, 0xAA, 0x51, 0x7E, 0xF6, 0xE8, 0x23, 0x6B, 0xA4, 0x82,
     0x7E},
    {0xD0, 0xAA, 0xD8, 0xDD, 0x44, 0x10, 0xE1, 0xDB, 0xBA, 0xFB, 0x94, 0x3A, 0x7F, 0x41, 0x07,
     0x71},
    {0xD8, 0xF4, 0xF5, 0x2D, 0x5A, 0xF8, 0x29, 0x7B, 0xC7, 0x84, 0xD5, 0xCC, 0xEB, 0x2B, 0x55,
     0xC6},
    {0xF8, 0xE8, 0x02, 0x1B, 0x62, 0xD1, 0x07, 0x0E, 0x44, 0x78, 0xD2, 0xF6, 0xC3, 0x7E, 0x94,
     0x51},
    {0x61, 0xCA, 0x74, 0x14, 0x40, 0x24, 0x66, 0xE8, 0xA6, 0x39, 0x21, 0x1E, 0x55, 0xD5, 0x2E,
     0x69},
    {0xC7, 0xA2, 0x69, 0xEE, 0xAD, 0x10, 0xD3, 0x45, 0x7D, 0x38, 0x36, 0x85, 0x74, 0x2E, 0x74,
     0x14},
    {0x35, 0xA2, 0x4F, 0xBA, 0xF1, 0x5D, 0x12, 0x04, 0x74, 0x5A, 0x07, 0x6C, 0x06, 0x75, 0xA9,
     0xCB},
};

uint8_t KL_WithKDF_PLAINTEXT[8][16] = {
    {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE,
     0xFF},
    {0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF,
     0x00},
    {0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00,
     0x11},
    {0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00, 0x11,
     0x22},
    {0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00, 0x11, 0x22,
     0x33},
    {0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00, 0x11, 0x22, 0x33,
     0x44},
    {0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00, 0x11, 0x22, 0x33, 0x44,
     0x55},
    {0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55,
     0x66},
};

uint8_t KL7_DIN[8][16] = {
    {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE,
     0xFF},
    {0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF,
     0x00},
    {0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00,
     0x11},
    {0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00, 0x11,
     0x22},
    {0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00, 0x11, 0x22,
     0x33},
    {0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00, 0x11, 0x22, 0x33, 0x44,
     0x55},
    {0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55,
     0x66},
    {0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66,
     0x77},
};

/*CSA2 is only support descryption */
uint8_t CSA2_KEY[8] = {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef};
uint8_t CSA2_CIPHERTEXT[184] = {
    0x57, 0x9c, 0xac, 0x51, 0x46, 0x11, 0x19, 0xef, 0x45, 0x17, 0x7b, 0x10, 0x17, 0x7b, 0x10, 0xe2,
    0x96, 0x11, 0x2a, 0x57, 0x9c, 0xac, 0x51, 0x45, 0x17, 0x7b, 0x10, 0xe2, 0x96, 0x11, 0x2a, 0x57,
    0x9c, 0xac, 0x51, 0x46, 0x11, 0x19, 0xef, 0xe2, 0x96, 0x11, 0x2a, 0x57, 0x9c, 0xac, 0x51, 0x46,
    0x11, 0x19, 0xef, 0x45, 0x17, 0x7b, 0x10, 0x57, 0x9c, 0xac, 0x51, 0x46, 0x11, 0x19, 0xef, 0x45,
    0x17, 0x7b, 0x10, 0xe2, 0x96, 0x11, 0x2a, 0x46, 0x11, 0x19, 0xef, 0x45, 0x17, 0x7b, 0x10, 0xe2,
    0x96, 0x11, 0x2a, 0x57, 0x9c, 0xac, 0x51, 0x45, 0x17, 0x7b, 0x10, 0xe2, 0x96, 0x11, 0x2a, 0x57,
    0x9c, 0xac, 0x51, 0x46, 0x11, 0x19, 0xef, 0xec, 0xba, 0xfb, 0xe6, 0x64, 0xc7, 0xb9, 0x72, 0x2f,
    0xdf, 0x1f, 0x68, 0x87, 0x4e, 0x8a, 0x35, 0xec, 0x81, 0x28, 0x6c, 0xa3, 0xe9, 0x89, 0xbd, 0x97,
    0x9b, 0x0c, 0xb2, 0x84, 0xb2, 0x6a, 0xeb, 0x18, 0x74, 0xe4, 0x7c, 0xa8, 0x35, 0x8f, 0xf2, 0x23,
    0x78, 0xf0, 0x91, 0x44, 0x58, 0xc8, 0xe0, 0x0b, 0x26, 0x31, 0x68, 0x6d, 0x54, 0xea, 0xb8, 0x4b,
    0x91, 0xf0, 0xac, 0x9c, 0xac, 0x51, 0x46, 0x11, 0x19, 0xef, 0x45, 0x17, 0x7b, 0x10, 0x17, 0x7b,
    0x10, 0xe2, 0x96, 0x11, 0x2a, 0x57, 0x9c, 0xac};
uint8_t CSA2_PLAINTEXT[184] = {
    0x37, 0x78, 0x68, 0x20, 0x8c, 0xef, 0x51, 0x30, 0x0e, 0x1c, 0xb9, 0x7e, 0xec, 0xbf, 0x7d, 0xd7,
    0xe2, 0xdb, 0x5b, 0xc2, 0xeb, 0x57, 0x19, 0x33, 0x2d, 0x0f, 0x5d, 0xf0, 0x89, 0xc9, 0x4c, 0x0f,
    0x03, 0xbe, 0xb1, 0xc1, 0xe0, 0x4f, 0x9c, 0x7b, 0xca, 0xb4, 0x86, 0xfe, 0x77, 0x54, 0xbe, 0x67,
    0x24, 0xc0, 0x93, 0x63, 0xbe, 0x19, 0x78, 0x4e, 0x0a, 0x77, 0xae, 0x35, 0xc9, 0x59, 0xea, 0x0f,
    0x4f, 0x5e, 0x73, 0x7d, 0xaa, 0x40, 0x32, 0xd9, 0xcb, 0xab, 0x78, 0x49, 0x11, 0xa3, 0xe0, 0x8c,
    0x31, 0xb6, 0x88, 0x31, 0x6b, 0x8b, 0xf3, 0x6b, 0xd6, 0x3b, 0xe1, 0xc9, 0x5e, 0x8e, 0x2c, 0x3f,
    0x87, 0x15, 0xcf, 0x32, 0xed, 0xa2, 0x31, 0x3b, 0xcc, 0x1f, 0xbc, 0x46, 0xaf, 0xef, 0xce, 0xb2,
    0x83, 0x0a, 0x90, 0x1b, 0xa9, 0x31, 0x02, 0xab, 0x18, 0x18, 0x4c, 0xe2, 0x6b, 0xef, 0xad, 0x3e,
    0xe6, 0xcc, 0x8c, 0xcb, 0x14, 0x3a, 0x97, 0x7e, 0x11, 0x8c, 0x8c, 0x68, 0x8e, 0x5d, 0x3b, 0x18,
    0x4d, 0xb7, 0x82, 0xe5, 0xd2, 0xab, 0xb6, 0x0d, 0xf4, 0xe1, 0xee, 0x33, 0x1a, 0xff, 0x19, 0x97,
    0x2e, 0xe9, 0xe7, 0x3e, 0x07, 0xa5, 0xbd, 0x83, 0xac, 0x29, 0xfb, 0x20, 0x1b, 0xb3, 0x98, 0x89,
    0xb3, 0x01, 0x1d, 0xd2, 0x13, 0xae, 0x7a, 0x1f};
uint8_t CSA3_KEY[16] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
                                     0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f};
uint8_t CSA3_CIPHERTEXT[184] = {
    0x14, 0x5c, 0x5d, 0x60, 0xde, 0x00, 0x06, 0xbd, 0x87, 0xed, 0xbe, 0x80, 0x55, 0xb7, 0xef, 0xe3,
    0xa1, 0x29, 0x1a, 0x52, 0xb8, 0x22, 0xdb, 0x12, 0xa1, 0x8a, 0x6b, 0x1a, 0x9f, 0x2b, 0x4c, 0xd8,
    0x9b, 0x68, 0x38, 0x39, 0x1e, 0xef, 0xb5, 0x4f, 0xf7, 0xa2, 0x7d, 0xb9, 0x5a, 0xca, 0xe2, 0xdc,
    0x33, 0x44, 0xab, 0x2f, 0x58, 0xb7, 0xb0, 0x85, 0x26, 0x68, 0x5b, 0x67, 0xa0, 0x28, 0x33, 0x4e,
    0xab, 0x11, 0xf2, 0x37, 0x7b, 0xa6, 0xbc, 0x54, 0xe0, 0x26, 0x02, 0x3d, 0x54, 0x57, 0x61, 0xd2,
    0x76, 0x62, 0xc0, 0xce, 0x04, 0xb4, 0xc8, 0x63, 0x2f, 0x4c, 0x2e, 0x8c, 0x89, 0x95, 0x37, 0xad,
    0x9a, 0xdb, 0x2a, 0x05, 0x2a, 0x7a, 0xb6, 0x75, 0x3f, 0x8b, 0x3c, 0x2a, 0xa4, 0x0f, 0x4b, 0x42,
    0x4e, 0xe7, 0x51, 0x2a, 0xe4, 0xf5, 0x9a, 0x6a, 0x64, 0x4b, 0x4a, 0x4d, 0x27, 0x82, 0x7f, 0x2b,
    0x48, 0xfe, 0x38, 0x0f, 0x5b, 0x6d, 0xe3, 0x2d, 0x94, 0x84, 0x47, 0x44, 0xd7, 0xd7, 0x7d, 0xf3,
    0x0c, 0xfe, 0xed, 0x8c, 0xb6, 0x77, 0xe9, 0x88, 0x00, 0xeb, 0xc6, 0x78, 0x3c, 0x09, 0x66, 0x55,
    0x9e, 0x01, 0x97, 0x8f, 0xc0, 0xdc, 0xdd, 0x2e, 0x6f, 0xdf, 0x85, 0x5e, 0xfb, 0xbd, 0x73, 0x91,
    0xf2, 0x28, 0x94, 0x33, 0x90, 0xfa, 0xdb, 0xbd};
uint8_t CSA3_PLAINTEXT[184] = {
    0xb7, 0xb6, 0xb5, 0xb4, 0xb3, 0xb2, 0xb1, 0xb0, 0xaf, 0xae, 0xad, 0xac, 0xab, 0xaa, 0xa9, 0xa8,
    0xa7, 0xa6, 0xa5, 0xa4, 0xa3, 0xa2, 0xa1, 0xa0, 0x9f, 0x9e, 0x9d, 0x9c, 0x9b, 0x9a, 0x99, 0x98,
    0x97, 0x96, 0x95, 0x94, 0x93, 0x92, 0x91, 0x90, 0x8f, 0x8e, 0x8d, 0x8c, 0x8b, 0x8a, 0x89, 0x88,
    0x87, 0x86, 0x85, 0x84, 0x83, 0x82, 0x81, 0x80, 0x7f, 0x7e, 0x7d, 0x7c, 0x7b, 0x7a, 0x79, 0x78,
    0x77, 0x76, 0x75, 0x74, 0x73, 0x72, 0x71, 0x70, 0x6f, 0x6e, 0x6d, 0x6c, 0x6b, 0x6a, 0x69, 0x68,
    0x67, 0x66, 0x65, 0x64, 0x63, 0x62, 0x61, 0x60, 0x5f, 0x5e, 0x5d, 0x5c, 0x5b, 0x5a, 0x59, 0x58,
    0x57, 0x56, 0x55, 0x54, 0x53, 0x52, 0x51, 0x50, 0x4f, 0x4e, 0x4d, 0x4c, 0x4b, 0x4a, 0x49, 0x48,
    0x47, 0x46, 0x45, 0x44, 0x43, 0x42, 0x41, 0x40, 0x3f, 0x3e, 0x3d, 0x3c, 0x3b, 0x3a, 0x39, 0x38,
    0x37, 0x36, 0x35, 0x34, 0x33, 0x32, 0x31, 0x30, 0x2f, 0x2e, 0x2d, 0x2c, 0x2b, 0x2a, 0x29, 0x28,
    0x27, 0x26, 0x25, 0x24, 0x23, 0x22, 0x21, 0x20, 0x1f, 0x1e, 0x1d, 0x1c, 0x1b, 0x1a, 0x19, 0x18,
    0x17, 0x16, 0x15, 0x14, 0x13, 0x12, 0x11, 0x10, 0x0f, 0x0e, 0x0d, 0x0c, 0x0b, 0x0a, 0x09, 0x08,
    0x07, 0x06, 0x05, 0x04, 0x03, 0x02, 0x01, 0x00};

/* Mac */
uint8_t key_cmac[16] = {0xe6, 0x6b, 0xca, 0x84, 0x94, 0xb9, 0xa0, 0x55,
							 0x0c, 0x2c, 0x50, 0x89, 0x02, 0x18, 0x51, 0xf7};

uint8_t key_aes128[16] = {0x61, 0x6c, 0x67, 0x6f, 0x20, 0x41, 0x45, 0x53,
                                 0x61, 0x6c, 0x67, 0x6f, 0x20, 0x61, 0x65, 0x73};
uint8_t key_tdes128[16] = {0x61, 0x6c, 0x67, 0x6f, 0x54, 0x44, 0x45, 0x53,
                                  0x61, 0x6c, 0x67, 0x6f, 0x74, 0x64, 0x65, 0x73};
uint8_t key_des[8] = {0x61, 0x6c, 0x67, 0x6f, 0x20, 0x44, 0x45, 0x53};
uint8_t iv1[16] = {0x49, 0x56, 0x30, 0x31, 0x69, 0x76, 0x30, 0x31,
                          0x49, 0x56, 0x30, 0x31, 0x69, 0x76, 0x30, 0x31};
uint8_t iv2[16] = {0x49, 0x56, 0x30, 0x32, 0x69, 0x76, 0x30, 0x32,
                          0x49, 0x56, 0x30, 0x32, 0x69, 0x76, 0x30, 0x32};

uint8_t syskey[32] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                             0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                             0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
// clang-format on
